<?php 
namespace App\core;

class App
{
	protected static $registry = [];
	 
	public static function bind($key, $value)
	{
		static::registry[$key] = $value; //cannot use temporary expression in write context .. error in php7l

	}

	public static function get($key)
	{
		if (! array_key_exists($key, static::registry)) {

			throw new Exception("No {key} is bound in the container");
			
		}
		return static::registry[$key];

	}
}

 ?>